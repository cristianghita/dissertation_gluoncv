import xml.etree.cElementTree as ET
import os
from DetectorResults import DetectorResults


def main():
    detector_results = []
    for filename in os.listdir('xml'):
        if filename.endswith('.xml'):
            video_name = filename[filename.index('-') + 1:-4]
            detector = filename[:filename.index('-')]
            fps, frames_dict = parse_xml('xml/' + filename, video_name)
            dr = DetectorResults(detector, video_name, fps, frames_dict)
            detector_results.append(dr)
    print(len(detector_results))
    for dr in detector_results:
        print(dr.detector, dr.video_name, dr.fps)


def create_xml_for_video(detector, frames_dict, video_name, fps):
    root = ET.Element('root')
    ET.SubElement(root, 'fps').text = str(fps)
    video = ET.SubElement(root, video_name, detector=detector)

    for key, value in frames_dict.items():
        ET.SubElement(video, 'frame', count=str(key)).text = str(value)

    tree = ET.ElementTree(root)
    tree.write('xml/' + detector + '-' + video_name + '.xml')


def parse_xml(filename, video_name):
    root = ET.parse(filename).getroot()
    frames_dict = {}
    for type_tag in root.findall(video_name + '/frame'):
        frame_count = int(type_tag.get('count'))
        frames_dict[frame_count] = int(type_tag.text)
    fps = root.find('fps').text
    # detector = root.find(video_name).get('detector')
    return fps, frames_dict


if __name__ == '__main__':
    main()
