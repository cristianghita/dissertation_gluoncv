from matplotlib import pyplot as plt
from mxnet import gpu
from gluoncv import model_zoo, data, utils

net = model_zoo.get_model('faster_rcnn_resnet50_v1b_voc', pretrained=True, ctx=gpu(0))

#im_fname = utils.download('https://github.com/dmlc/web-data/blob/master/' +
#                          'gluoncv/detection/biking.jpg?raw=true',
#                          path='biking.jpg')

im_fname = utils.download('https://raw.githubusercontent.com/zhreshold/' +
                          'mxnet-ssd/master/data/demo/dog.jpg',
                          path='dog.jpg')

x, orig_img = data.transforms.presets.rcnn.load_test(im_fname)

box_ids, scores, bboxes = net(x.as_in_context(gpu(0)))
ax = utils.viz.plot_bbox(orig_img, bboxes[0], scores[0], box_ids[0],
                         thresh=0.8,
                         class_names=net.classes)

plt.show()

