class DetectorResults:
    def __init__(self, detector, video_name, fps, frames_dict):
        self.detector = detector
        self.video_name = video_name
        self.fps = fps
        self.frames_dict = frames_dict
        self.persons_array = []
        self.frames_array = list(range(len(frames_dict)))
        for i in range(len(frames_dict)):
            self.persons_array.append(frames_dict[i])
        self.perfect_match_frames = 0
        self.persons_accuracy = 0
