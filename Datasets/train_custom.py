import gluoncv as gcv
from gluoncv.data import VOCDetection
from gluoncv.data.transforms import presets
from gluoncv import utils
from mxnet import nd
from gluoncv.data.batchify import Tuple, Stack, Pad
from mxnet.gluon.data import DataLoader
import mxnet as mx
from mxnet import autograd
from gluoncv import model_zoo

batch_size = 2  # small for tutorial
num_workers = 0  # make it larger if your CPUhas more cores to accelerate data loading

train_dataset = VOCDetection(root='D:\\Others\\University\\Dissertation\\Dataset', splits=[(2018, 'train')])
val_dataset = VOCDetection(root='D:\\Others\\University\\Dissertation\\Dataset', splits=[(2018, 'test')])

print('Training images: ', len(train_dataset))
print('Validation images: ', len(val_dataset))

width, height = 416, 416  # resize the image to 416x416 after all data augmentation
train_transform = presets.yolo.YOLO3DefaultTrainTransform(width, height)
val_transform = presets.yolo.YOLO3DefaultTrainTransform(width, height)

utils.random.seed(123)  # fix seed in this tutorial

batchify_fn = Tuple(Stack(), Pad(pad_val=-1))
train_loader = DataLoader(train_dataset.transform(train_transform), batch_size, shuffle=True,
                          batchify_fn=batchify_fn, last_batch='rollover', num_workers=num_workers)
val_loader = DataLoader(val_dataset.transform(val_transform), batch_size, shuffle=False,
                        batchify_fn=batchify_fn, last_batch='keep', num_workers=num_workers)

# for train_image, train_label in train_dataset:
    # tensor_train_image, tensor_train_label = train_transform(train_image, train_label)

for ib, batch in enumerate(train_loader):
    if ib > 3:
        break
    print('data 0:', batch[0][0].shape, 'label 0:', batch[1][0].shape)
    print('data 1:', batch[0][1].shape, 'label 0:', batch[1][1].shape)

net = model_zoo.get_model('yolo3_darknet53_voc', pretrained_base=False)

x = mx.nd.zeros(shape=(1, 3, 416, 416))
net.initialize()
cids, scores, bboxes = net(x)  # class_labels, confidence_scores, bounding_boxes

loss = gcv.loss.YOLOV3Loss()

train_transform = presets.yolo.YOLO3DefaultTrainTransform(width, height, net)
batchify_fn = Tuple(*([Stack() for _ in range(6)] + [Pad(axis=0, pad_val=-1) for _ in range(1)]))
train_loader = DataLoader(train_dataset.transform(train_transform), batch_size, shuffle=True,
                          batchify_fn=batchify_fn, last_batch='rollover', num_workers=num_workers)


