from gluoncv.data import VOCDetection


class VOCLike(VOCDetection):
    CLASSES = ['person']

    def __init__(self, root, splits, transform=None, index_map=None, preload_label=True):
        super(VOCLike, self).__init__(root, splits, transform, index_map, preload_label)


dataset = VOCLike(root='D:\\Others\\University\\Dissertation\\Dataset', splits=((2018, 'train'),))
print('length of dataset: ', len(dataset))
print('label example: ')
print(dataset[0][1])
