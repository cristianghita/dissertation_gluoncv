from gluoncv import data, utils
from matplotlib import pyplot as plt

train_dataset = data.VOCDetection(root='D:\\gluoncv\\Datasets\\PASCAL VOC\\voc', splits=[(2007, 'trainval'), (2012, 'trainval')])
val_dataset = data.VOCDetection(root='D:\\gluoncv\\Datasets\\PASCAL VOC\\voc', splits=[(2007, 'test')])

print('Num of training images:', len(train_dataset))
print('Num of validation images:', len(val_dataset))

train_image, train_label = train_dataset[5]
print('Image size (height, width, RGB):', train_image.shape)
print('')
print(train_dataset[5])

bounding_boxes = train_label[:, :4]