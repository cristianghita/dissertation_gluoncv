import time
import cv2
import gluoncv as gcv
import matplotlib.pyplot as plt
import mxnet as mx
import XmlService as XS

detector = 'ssd_coco_mobilenet'
video_names = ['hq_c_bg', 'hq_c_ct', 'hq_c_rg', 'lq_bw_sw', 'lq_bw_tg', 'lq_c_mk', 'lq_c_sb']
video_paths = [
    'video/High Quality - Color - Beard guy.mp4',
    'video/High Quality - Color - Couple trespassing.mp4',
    'video/High Quality - Color - Random guy.mp4',
    'video/Low Quality - B&W - Solo woman.mp4',
    'video/Low Quality - B&W - Three guys.mp4',
    'video/Low Quality - Color - Mean kids.mp4',
    'video/Low Quality - Color - Skate & Bike.mp4'
]
axes = None
short, max_size = 512, 700


def main():
    # Load the model
    net = gcv.model_zoo.get_model('ssd_512_mobilenet1.0_coco', pretrained=True, ctx=mx.gpu(0))
    for i in range(len(video_paths)):
        video_name = video_names[i]
        video_path = video_paths[i]
        run_for_video(video_name=video_name, video_path=video_path, net=net)


def run_for_video(video_name, video_path, net):
    print(' ## START FOR VIDEO [' + video_path + '] ## ')
    fps = get_detector_fps(net, video_path)
    frames_dict = get_detector_persons(net, video_path)
    XS.create_xml_for_video(detector, frames_dict, video_name, fps)


def load_video(video_path):
    # cap = cv2.VideoCapture(0)
    cap = cv2.VideoCapture(video_path)
    return cap


def get_detector_persons(net, video_path):
    frames_dict = {}
    cap = load_video(video_path)
    num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    for i in range(num_frames):
        # Load frame from the camera
        ret, frame = cap.read()

        # Image pre-processing
        frame = mx.nd.array(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)).astype('uint8')
        rgb_nd, frame = gcv.data.transforms.presets.ssd.transform_test(frame, short=short, max_size=max_size)

        # Run frame through network
        class_IDs, scores, bounding_boxes = net(rgb_nd.as_in_context(mx.gpu(0)))

        # Threshold 0.5
        high_scores = [p for p in range(0, len(scores[0])) if scores[0][p] > 0.5 and
                       net.classes[int((class_IDs[0][p].asnumpy())[0])].upper() == 'PERSON']
        print('Persons detected in frame [' + str(i) + ']: ' + str(len(high_scores)))

        frames_dict[i] = len(high_scores)
        # for j in range(0, len(scores[0])):
        #    if scores[0][j] > 0.5:
        #        classId = int((class_IDs[0][j].asnumpy())[0])  # personId = 14
        #        classScore = (scores[0][j].asnumpy())[0]
        #        className = net.classes[classId]
        #        print('FRAME ' + str(i) + ' ' + str(className)+ '[' + str(classId) + '] ' + str(classScore))

        # Display the result
        # plt.cla()
        # axes = gcv.utils.viz.plot_bbox(frame, bounding_boxes[0], scores[0], class_IDs[0], class_names=net.classes, ax=axes)
        # plt.draw()
        # plt.pause(0.001)
    cap.release()
    return frames_dict


def get_detector_fps(net, video_path):
    cap = load_video(video_path)
    num_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    print('Total no of frames: ' + str(num_frames))
    t0 = time.time()
    for i in range(num_frames):
        ret, frame = cap.read()
        frame = mx.nd.array(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)).astype('uint8')
        rgb_nd, frame = gcv.data.transforms.presets.ssd.transform_test(frame, short=short, max_size=max_size)
        net(rgb_nd.as_in_context(mx.gpu(0)))
    t1 = time.time()
    fps = int(num_frames / (t1 - t0))
    print('FPS: ' + str(int(fps)))
    cap.release()
    return fps


if __name__ == '__main__':
    main()

