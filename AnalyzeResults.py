from DetectorResults import DetectorResults
import XmlService as XS
import matplotlib.pyplot as plt
import os
import numpy as np
import operator

video_names = [
    # 'hq_c_bg',
    # 'hq_c_ct',
    # 'hq_c_rg',
    # 'lq_bw_sw',
    # 'lq_bw_tg',
    # 'lq_c_mk',
    'lq_c_sb'
    ]


def main():
    detector_results = parse_xml_files()
    detector_results = extract_acc_proc(detector_results)
    generate_graphs(detector_results)
    generate_barh_graph(detector_results)
    generate_barh_graph_fp(detector_results)
    rank(detector_results)


def rank(detector_results):
    for video_name in video_names:
        acc_rank = {}
        rrank = {'first': 0, 'second': 0, 'third': 0}
        for dr in detector_results:
            if dr.video_name == video_name:
                acc_rank[dr.detector] = dr.perfect_match_frames - dr.persons_accuracy
        print(video_name)
        sorted_acc_rank = sorted(acc_rank.items(), key=lambda kv: kv[1])
        print(sorted_acc_rank)


def generate_barh_graph_fp(detector_results):
    for video_name in video_names:
        plt.figure()
        objects = []
        performance = []
        for dr in detector_results:
            if dr.video_name == video_name:
                objects.append(dr.detector)
                if dr.detector.find('hardcoded') != -1:
                    performance.append(dr.persons_accuracy)
                else:
                    performance.append(dr.persons_accuracy)
        y_pos = np.arange(len(objects))
        plt.barh(y_pos, performance, color='darkred')
        plt.yticks(y_pos, objects)
        plt.xlabel('Total of false positives + misses')
        plt.title(video_name)
        plt.show()


def generate_barh_graph(detector_results):
    for video_name in video_names:
        plt.figure()
        objects = []
        performance = []
        for dr in detector_results:
            if dr.video_name == video_name:
                objects.append(dr.detector)
                if dr.detector.find('hardcoded') != -1:
                    performance.append(len(dr.frames_dict))
                else:
                    performance.append(dr.perfect_match_frames)
        y_pos = np.arange(len(objects))
        plt.barh(y_pos, performance, color='green')
        plt.yticks(y_pos, objects)
        plt.xlabel('Frames')
        plt.title(video_name)
        plt.show()


def extract_acc_proc(detector_results):
    hardcoded_frames_dict = {}
    for video_name in video_names:
        for dr in detector_results:
            if dr.video_name == video_name and dr.detector.find('hardcoded') != -1:
                hardcoded_frames_dict[video_name] = dr.frames_dict

    for video_name in video_names:
        for dr in detector_results:
            if dr.video_name == video_name:
                if dr.detector.find('hardcoded') == -1:
                    hardcoded_dict_for_video = hardcoded_frames_dict[video_name]
                    for i in range(len(hardcoded_dict_for_video)):
                        if hardcoded_dict_for_video[i] == dr.frames_dict[i]:
                            dr.perfect_match_frames += 1
                        dr.persons_accuracy += abs(hardcoded_frames_dict[video_name][i] - dr.frames_dict[i])
                    # print(video_name, dr.detector, dr.perfect_match_frames, dr.persons_accuracy,
                    #       '-',
                    #       str(int(dr.perfect_match_frames/len(dr.frames_dict) * 100)) + '%',
                    #       '-',
                    #       str(dr.perfect_match_frames) + '/' + str(len(dr.frames_dict)))

    return detector_results


def generate_graphs(detector_results):
    for video_name in video_names:
        plt.figure()
        for dr in detector_results:
            if dr.video_name == video_name:
                if dr.detector.find('hardcoded') != -1:
                    plt.plot(dr.frames_array, dr.persons_array, label=dr.detector, linewidth=7, alpha=0.5)
                else:
                    plt.plot(dr.frames_array, dr.persons_array, label=dr.detector, linestyle='dashed', alpha=0.8)
        plt.legend(loc='best')
        plt.title(video_name)
        plt.show()


def parse_xml_files():
    detector_results = []
    for filename in os.listdir('xml'):
        if filename.endswith('.xml'):
            video_name = filename[filename.index('-') + 1:-4]
            detector = filename[:filename.index('-')]
            fps, frames_dict = XS.parse_xml('xml/' + filename, video_name)
            dr = DetectorResults(detector, video_name, fps, frames_dict)
            detector_results.append(dr)

    # for dr in detector_results:
    #     print(dr.detector, dr.video_name, dr.fps)
    return detector_results


if __name__ == '__main__':
    main()
